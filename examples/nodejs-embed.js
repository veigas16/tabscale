/**
 * Unlike the other examples, this represents a PARTIAL example. This code would, typically
 * be used along with a bundler (Webpack, e.g.) to transpile and bundle code before it is
 * used by a front-end solution. To make use of this method, you'll need to add the
 * library using npm or yarn.
 */
import TabScale from 'tabscale';

// This link is provided by Tableau Public when you click on the share button
const vizUrl = 'https://public.tableau.com/views/SuperstoreDashboard_14/SSDashboard?:embed=y&:display_count=yes&:origin=viz_share_link';
var viz;
const tabScale = new TabScale.Scale(document.getElementById('tableauViz'));
// scale the viz horizontally and from the bottom right
tabScale.scaleDirection = TabScale.Options.ScaleDirection.Horizontal;
tabScale.scaleFrom = TabScale.Options.ScaleFrom.BottomRight;

const loadViz = () => {
    let containerViz = document.getElementById('tableauViz'),
        options = {
            onFirstInteractive: () => {
                tabScale.initialize();
            }
        }

    viz = new tableau.Viz(containerViz, vizUrl, options);
}

loadViz();