# Examples

These examples illustrate a few of the ways that TabScale can be utilized in your project.

## Overview

### Visualization

For the purpose of consistency, we will use my colleague Jon Dugger's reimagining of [Superstore](https://public.tableau.com/profile/jodugg3205#!/vizhome/SuperstoreDashboard_14/SSDashboard) from Tableau Public. Tableau Public requires no authentication but, in terms of embedding, it functions exactly as Tableau Server or Online would.

### Usage Guide

Assuming you've cloned this repo, both the `simple-embed.html` and `inline-embed.html` files can be opened in any modern browser as-is. The `nodejs-embed.js` example requires a front-end dev workflow utilizing Node JS. Generally this means you'll add the library using either `npm` or `yarn` and import it into your code.

### Complexity

Each of these examples is meant to replicate the exact same result using different workflows. In general order of complexity and functionality (the more complex, the more functionality you have access to), the approaches could be ranked as:

1. `simple-embed.html`
2. `inline-embed.html`
3. `nodejs-embed.js`

So, if you are completely new to Javascript and/or front-end development, you'll likely want to start with the approach presented in `simple-embed.html`. If you are familiar with front-end development and already have an established workflow, feel free to jump into the `nodejs-embed.js` version.

## Simple Embedding

Simple embedding is meant to be a _mostly_ hands-off approach to implementing this library. This approach will be a good option when you are embedding your visualization using the Embed Code provided by Tableau Server, Online, or Public after you click the share button.

The idea behind simple embedding is to minimize the amount of manual manipulation that has to happen in your HTML. Generally, you should be able to use this library by adding a reference to it and adding an `onload` event in the body tag of your HTML.

## Inline Embedding

Inline embedding includes use of the Tableau JS API and requires some degree of comfort writing Javascript. In this approach, you have access to more functionality in terms of both embedding and using TabScale. If you are just getting started writing JS, this might be the best approach for you.

## Node JS Embedding

This is the most complex and functional version of using this library and is meant for individuals who are very familiar with front-end development. This approach assumes you have a development workflow established where you are bundling code, transpiling, etc.