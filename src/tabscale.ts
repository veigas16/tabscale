module TabScale {

    export class Scale {

        /** 
         * Allows for granular control of the CSS transform-origin property. If this value
         * is set, it overrides any value set on the `scaleFrom` property.
         */
        transformOrigin: string = undefined;
        /** Simple, predefined options for controling how the visualization is scaled. */
        scaleFrom: ScaleFrom = Options.ScaleFrom.BottomRight
        /** 
         * Determines whether scaling occurs in response to changes in the horizontal 
         * or vertical window size. */
        scaleDirection: ScaleDirection = Options.ScaleDirection.Horizontal;
        /** 
         * Width, in pixels to be used to account for the browser's scroll bar. If this
         * is not used or set incorrectly, the scrollbar will appear to overlap part of
         * the visualization.
        */
        scrollbarPixelWidth: number = 17;
        /** 
         * Sets a lower boundary, in pixels, where scaling will stop. Use this to prevent
         * users from scaling the viz to a size where it's no longer useful. 
         */
        minScalePixels: number = undefined;

        private domVizContainer: HTMLElement;
        private domVizIframe: HTMLElement;

        constructor(vizContainerElement: HTMLElement) {
            this.domVizContainer = vizContainerElement;
        }

        /** 
         * Initializes the scaling behavior by setting event listeners and using any 
         * user-defined configuration options provided. This method should be called **after**
         * the visualization is embedded. The recommended option is to call this in the
         * `onFirstInteractive` option in Tableau's JS API.
         */
        initialize = (): void => {
            const childIframes: HTMLCollectionOf<HTMLIFrameElement> = this.domVizContainer.getElementsByTagName('iframe');
            if (childIframes.length !== 1) {
                throw new ReferenceError('The DOM element where you are embedding your visualization must contain EXACTLY one <iframe> element.');
            }
            this.domVizIframe = childIframes[0];
            this.domVizContainer.style.transformOrigin = this.transformOrigin || this.scaleFrom;
            window.addEventListener('resize', this.scale);
            this.scale();
        }

        /**
         * Forces the viz to scale based on current window size. Should not be called before
         * the `initialize` method is invoked. Generally, this low-level method shouldn't be
         * used but there may be instances where a user to needs to force a resize.
         */
        scale = (): void => {
            this.scaleViz(this.scaleDirection);
        }

        private scaleViz = (scaleDirection: ScaleDirection): void => {
            let vizPixels: number,
                windowPixels: number;
            switch (scaleDirection) {
                case Options.ScaleDirection.Horizontal:
                    vizPixels = parseInt(this.domVizIframe.style.width);
                    windowPixels = window.innerWidth - this.scrollbarPixelWidth;
                    break;
                case Options.ScaleDirection.Vertical:
                    vizPixels = parseInt(this.domVizIframe.style.height);
                    windowPixels = window.innerHeight;
                    break;
            }
            if (scaleDirection === Options.ScaleDirection.Horizontal) {
                vizPixels = parseInt(this.domVizIframe.style.width);
            }
            if (windowPixels >= vizPixels) {
                this.domVizContainer.style.transform = 'scale(1)';
            } else {
                if (this.minScalePixels) {
                    if (windowPixels > this.minScalePixels) {
                        this.domVizContainer.style.transform = `scale(${windowPixels / vizPixels})`;        
                    }
                } else {
                    this.domVizContainer.style.transform = `scale(${windowPixels / vizPixels})`;
                }
            }
        }
    }

    /**
     * Used to handle simple embedding scenario, in particular those cases where a user
     * wants to leverage the Embed Code provided by Tableau Server, Online, or Public.
     * This method uses a sensible set of defaults that should be sufficient for the
     * vast majority of simple embed use cases but allows no configuration or
     * customization.
     * 
     * Example:
     * ```html
     * <!DOCTYPE html>
     * <html>
     *    <head>
     *        <title>Tableau Embedded - Dynamic Scaling</title>
     *        <!-- Add a reference to the tabscale lib -->
     *        <script type="text/javascript" src="./tabscale.js"></script>
     *    </head>
     *    <!-- On load, call the handleScaling function and provide the div ID where the viz is embedded -->
     *    <body onload="javascript: TabScale.handleScaling('tableauViz');">
     *        <div>Tableau Embedded - Dynamic Scaling</div>
     *        <div id="tableauViz">
     *            <!-- EMBED CODE FROM TABLEAU GOES HERE -->
     *        </div>
     *    </body>
     * </html>
     * ````
     */
    export const handleScaling = (nodeId: string): void => {
        const targetNode: HTMLElement = document.getElementById(nodeId);
        const observerCallback = (mutationsList: MutationRecord[], observer: MutationObserver): void => {
            let mutation: MutationRecord;
            for (mutation of mutationsList) {
                console.log(mutation);
                if (mutation.addedNodes.length > 0) {
                    mutation.addedNodes.forEach((node: HTMLElement) => {
                        if (node.tagName.toUpperCase() === 'IFRAME') {
                            let tabscale = new Scale(targetNode);
                            tabscale.initialize()
                        }
                    })
                }
            }
        }
        const observer: MutationObserver = new MutationObserver(observerCallback);
        observer.observe(targetNode, {
            childList: true,
            subtree: true
        });
    }

    enum ScaleFrom {
        TopLeft = '50% 50%',
        TopRight = '0% 50%',
        BottomLeft = '50% 0%',
        BottomRight = '0% 0%'
    }

    enum ScaleDirection {
        Horizontal,
        Vertical
    }

    export const Options = {
        ScaleFrom: ScaleFrom,
        ScaleDirection: ScaleDirection
    }

}

export default TabScale;