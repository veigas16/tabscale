declare module TabScale {
    export class Scale {
        /**
         * Allows for granular control of the CSS transform-origin property. If this value
         * is set, it overrides any value set on the `scaleFrom` property.
         */
        transformOrigin: string;
        /** Simple, predefined options for controling how the visualization is scaled. */
        scaleFrom: ScaleFrom;
        /**
         * Determines whether scaling occurs in response to changes in the horizontal
         * or vertical window size. */
        scaleDirection: ScaleDirection;
        /**
         * Width, in pixels to be used to account for the browser's scroll bar. If this
         * is not used or set incorrectly, the scrollbar will appear to overlap part of
         * the visualization.
        */
        scrollbarPixelWidth: number;
        /**
         * Sets a lower boundary, in pixels, where scaling will stop. Use this to prevent
         * users from scaling the viz to a size where it's no longer useful.
         */
        minScalePixels: number;
        private domVizContainer;
        private domVizIframe;
        constructor(vizContainerElement: HTMLElement);
        /**
         * Initializes the scaling behavior by setting event listeners and using any
         * user-defined configuration options provided. This method should be called **after**
         * the visualization is embedded. The recommended option is to call this in the
         * `onFirstInteractive` option in Tableau's JS API.
         */
        initialize: () => void;
        /**
         * Forces the viz to scale based on current window size. Should not be called before
         * the `initialize` method is invoked. Generally, this low-level method shouldn't be
         * used but there may be instances where a user to needs to force a resize.
         */
        scale: () => void;
        private scaleViz;
    }
    /**
     * Used to handle simple embedding scenario, in particular those cases where a user
     * wants to leverage the Embed Code provided by Tableau Server, Online, or Public.
     * This method uses a sensible set of defaults that should be sufficient for the
     * vast majority of simple embed use cases but allows no configuration or
     * customization.
     *
     * Example:
     * ```html
     * <!DOCTYPE html>
     * <html>
     *    <head>
     *        <title>Tableau Embedded - Dynamic Scaling</title>
     *        <!-- Add a reference to the tabscale lib -->
     *        <script type="text/javascript" src="./tabscale.js"></script>
     *    </head>
     *    <!-- On load, call the handleScaling function and provide the div ID where the viz is embedded -->
     *    <body onload="javascript: TabScale.handleScaling('tableauViz');">
     *        <div>Tableau Embedded - Dynamic Scaling</div>
     *        <div id="tableauViz">
     *            <!-- EMBED CODE FROM TABLEAU GOES HERE -->
     *        </div>
     *    </body>
     * </html>
     * ````
     */
    export const handleScaling: (nodeId: string) => void;
    enum ScaleFrom {
        TopLeft = "50% 50%",
        TopRight = "0% 50%",
        BottomLeft = "50% 0%",
        BottomRight = "0% 0%"
    }
    enum ScaleDirection {
        Horizontal = 0,
        Vertical = 1
    }
    export const Options: {
        ScaleFrom: typeof ScaleFrom;
        ScaleDirection: typeof ScaleDirection;
    };
    export {};
}
export default TabScale;
