var TabScale;
(function (TabScale) {
    var Scale = /** @class */ (function () {
        function Scale(vizContainerElement) {
            var _this = this;
            /**
             * Allows for granular control of the CSS transform-origin property. If this value
             * is set, it overrides any value set on the `scaleFrom` property.
             */
            this.transformOrigin = undefined;
            /** Simple, predefined options for controling how the visualization is scaled. */
            this.scaleFrom = TabScale.Options.ScaleFrom.BottomRight;
            /**
             * Determines whether scaling occurs in response to changes in the horizontal
             * or vertical window size. */
            this.scaleDirection = TabScale.Options.ScaleDirection.Horizontal;
            /**
             * Width, in pixels to be used to account for the browser's scroll bar. If this
             * is not used or set incorrectly, the scrollbar will appear to overlap part of
             * the visualization.
            */
            this.scrollbarPixelWidth = 17;
            /**
             * Sets a lower boundary, in pixels, where scaling will stop. Use this to prevent
             * users from scaling the viz to a size where it's no longer useful.
             */
            this.minScalePixels = undefined;
            /**
             * Initializes the scaling behavior by setting event listeners and using any
             * user-defined configuration options provided. This method should be called **after**
             * the visualization is embedded. The recommended option is to call this in the
             * `onFirstInteractive` option in Tableau's JS API.
             */
            this.initialize = function () {
                var childIframes = _this.domVizContainer.getElementsByTagName('iframe');
                if (childIframes.length !== 1) {
                    throw new ReferenceError('The DOM element where you are embedding your visualization must contain EXACTLY one <iframe> element.');
                }
                _this.domVizIframe = childIframes[0];
                _this.domVizContainer.style.transformOrigin = _this.transformOrigin || _this.scaleFrom;
                window.addEventListener('resize', _this.scale);
                _this.scale();
            };
            /**
             * Forces the viz to scale based on current window size. Should not be called before
             * the `initialize` method is invoked. Generally, this low-level method shouldn't be
             * used but there may be instances where a user to needs to force a resize.
             */
            this.scale = function () {
                _this.scaleViz(_this.scaleDirection);
            };
            this.scaleViz = function (scaleDirection) {
                var vizPixels, windowPixels;
                switch (scaleDirection) {
                    case TabScale.Options.ScaleDirection.Horizontal:
                        vizPixels = parseInt(_this.domVizIframe.style.width);
                        windowPixels = window.innerWidth - _this.scrollbarPixelWidth;
                        break;
                    case TabScale.Options.ScaleDirection.Vertical:
                        vizPixels = parseInt(_this.domVizIframe.style.height);
                        windowPixels = window.innerHeight;
                        break;
                }
                if (scaleDirection === TabScale.Options.ScaleDirection.Horizontal) {
                    vizPixels = parseInt(_this.domVizIframe.style.width);
                }
                if (windowPixels >= vizPixels) {
                    _this.domVizContainer.style.transform = 'scale(1)';
                }
                else {
                    if (_this.minScalePixels) {
                        if (windowPixels > _this.minScalePixels) {
                            _this.domVizContainer.style.transform = "scale(" + windowPixels / vizPixels + ")";
                        }
                    }
                    else {
                        _this.domVizContainer.style.transform = "scale(" + windowPixels / vizPixels + ")";
                    }
                }
            };
            this.domVizContainer = vizContainerElement;
        }
        return Scale;
    }());
    TabScale.Scale = Scale;
    /**
     * Used to handle simple embedding scenario, in particular those cases where a user
     * wants to leverage the Embed Code provided by Tableau Server, Online, or Public.
     * This method uses a sensible set of defaults that should be sufficient for the
     * vast majority of simple embed use cases but allows no configuration or
     * customization.
     *
     * Example:
     * ```html
     * <!DOCTYPE html>
     * <html>
     *    <head>
     *        <title>Tableau Embedded - Dynamic Scaling</title>
     *        <!-- Add a reference to the tabscale lib -->
     *        <script type="text/javascript" src="./tabscale.js"></script>
     *    </head>
     *    <!-- On load, call the handleScaling function and provide the div ID where the viz is embedded -->
     *    <body onload="javascript: TabScale.handleScaling('tableauViz');">
     *        <div>Tableau Embedded - Dynamic Scaling</div>
     *        <div id="tableauViz">
     *            <!-- EMBED CODE FROM TABLEAU GOES HERE -->
     *        </div>
     *    </body>
     * </html>
     * ````
     */
    TabScale.handleScaling = function (nodeId) {
        var targetNode = document.getElementById(nodeId);
        var observerCallback = function (mutationsList, observer) {
            var mutation;
            for (var _i = 0, mutationsList_1 = mutationsList; _i < mutationsList_1.length; _i++) {
                mutation = mutationsList_1[_i];
                console.log(mutation);
                if (mutation.addedNodes.length > 0) {
                    mutation.addedNodes.forEach(function (node) {
                        if (node.tagName.toUpperCase() === 'IFRAME') {
                            var tabscale = new Scale(targetNode);
                            tabscale.initialize();
                        }
                    });
                }
            }
        };
        var observer = new MutationObserver(observerCallback);
        observer.observe(targetNode, {
            childList: true,
            subtree: true
        });
    };
    var ScaleFrom;
    (function (ScaleFrom) {
        ScaleFrom["TopLeft"] = "50% 50%";
        ScaleFrom["TopRight"] = "0% 50%";
        ScaleFrom["BottomLeft"] = "50% 0%";
        ScaleFrom["BottomRight"] = "0% 0%";
    })(ScaleFrom || (ScaleFrom = {}));
    var ScaleDirection;
    (function (ScaleDirection) {
        ScaleDirection[ScaleDirection["Horizontal"] = 0] = "Horizontal";
        ScaleDirection[ScaleDirection["Vertical"] = 1] = "Vertical";
    })(ScaleDirection || (ScaleDirection = {}));
    TabScale.Options = {
        ScaleFrom: ScaleFrom,
        ScaleDirection: ScaleDirection
    };
})(TabScale || (TabScale = {}));
var TabScale$1 = TabScale;

export default TabScale$1;
